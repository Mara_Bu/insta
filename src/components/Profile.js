import React from 'react';
import User from './User';
import Palette from './Palette';

const Profile = () => {
    return (
        <div className="container profile">
            <User
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnKS0B8Kn4TRTS5sRJwMwk4wQtiyBWjzCjsCat-uqicwdRrSDaTg"
                alt="woman"
                name="Some_cool_woman"/>
                
                <Palette />
        </div>
    )
}

export default Profile;