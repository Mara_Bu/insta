import React, {Component} from 'react';
import User from './User.js';
import InstaService from './services/instaservice.js';
import ErrorMessage from './ErrorMessage.js';


export default class Users extends Component {
    InstaService = new InstaService();
    state = {
        users: [],
        error: false
    } 
    componentDidMount() {
        this.updateUsers();
    }

    updateUsers() {
        this.InstaService.getAllPosts()
        .then(this.onUsersLoaded)
        .catch(this.onError);
    }

    onUsersLoaded = (posts) => {
        this.setState({
            users:posts,
            error: false
        })
    }
    onError = () => {
        this.setState({
            error: true
        })
    }
    renderUsers(arr) {
        return (
            <div className="users__block"> {
                arr.map(item => {
                  const {name, altname, photo, id} = item;                 
                    return (
                      <User                       
                        src = {photo}
                        alt = {altname}
                        name = {name}
                        min
                      />
                    );                  
                })
              }
            </div>
          );
}

    render() {
        const {error, users} = this.state;
            if (error) {
                return <ErrorMessage/>
            }
            const item = this.renderUsers(users);

            return (
                

                <div className="rigth">
                    <User
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT8tRo5D0yrEUfjOepMIk4fzVGTTgqmx4dCtJ0y1MlZK36mOttp"
                alt="princessa"
                name="Name_princess"
            />
                    {item}
                </div>
            )
        }
    }